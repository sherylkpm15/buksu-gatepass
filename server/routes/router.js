const express = require('express');
const route = express.Router()

const controller = require('../controller/controller');
const auth = require('../middleware/auth');

var Admindb = require('../model/adminModel');
const axios = require('axios');


route.get('/', function (req, res) {
  res.render('index');
})

route.get('/register-account', function (req, res) {
  res.render('signup');
})

// HOME PAGE
route.get('/admin-homepage', auth, function (req, res) {
  res.render('admin_homepage');
})

// PROFILE ROUTES
route.get('/profile', auth, function (req, res) {
  Admindb.findById(req.userId, { password: 0 }, function (err, user) {
    if (err) return res.status(500).send("There was a problem finding the user.");
    if (!user) return res.status(404).send("No user found.");

    // sending the user data
    res.render('admin_profile', { user });
  });
});

route.get('/update-personal-infos', auth, function (req, res) {
  Admindb.findById(req.userId, function (err, user) {
    if (err) return res.status(500).send("There was a problem finding the user.");
    if (!user) return res.status(404).send("No user found.");

    res.render("admin_profilePersonalInfoUpdate", { user })

  });
});


//EVENTS ROUTES
route.get('/events', auth, function (req, res) {
    axios.get('http://localhost:3000/api/events')
      .then(function (response) {
        res.render('admin_events', { events: response.data });
      })
      .catch(err => {
        res.send(err);
      })
});

route.get('/event-add', auth, function (req, res) {
    res.render('admin_eventAdd');
});

route.get('/update-event', auth, function (req, res) {
    axios.get('http://localhost:3000/api/events', { params: { id: req.query.id } })
      .then(function (eventdata) {
        res.render("admin_eventUpdate", { events: eventdata.data })
      })
      .catch(err => {
        res.send(err);
      })
});

// USER ROUTES
route.get('/user-page', auth, function (req, res) {
    axios.get('http://localhost:3000/api/users')
      .then(function (response) {
        res.render('admin_userpage', { users: response.data });
      })
      .catch(err => {
        res.send(err);
      })
});

route.get('/add-user', auth, function (req, res) {
    res.render('admin_adduser');
});

route.get('/user-Records', auth, function (req, res) {
    axios.get('http://localhost:3000/api/users', { params: { id: req.query.id } })
      .then(function (userdata) {
        res.render("admin_userRecords", { users: userdata.data });
      })
      .catch(err => {
        res.send(err);
      })
});

route.get('/user-update', auth, function (req, res) {
    axios.get('http://localhost:3000/api/users', { params: { id: req.query.id } })
      .then(function (userdata) {
        res.render("admin_userPersonalInfoUpdate", { users: userdata.data })
      })
      .catch(err => {
        res.send(err);
      })
});

route.get('/user-update-license', auth, function (req, res) {
    axios.get('http://localhost:3000/api/users', { params: { id: req.query.id } })
      .then(function (userdata) {
        res.render("admin_userLicenseUpdate", { users: userdata.data })
      })
      .catch(err => {
        res.send(err);
      })
});

route.get('/user-update-vehicle', auth, function (req, res) {
    axios.get('http://localhost:3000/api/users', { params: { id: req.query.id } })
      .then(function (userdata) {
        res.render("admin_userVehicleUpdate", { users: userdata.data })
      })
      .catch(err => {
        res.send(err);
      })
});

route.get('/logout', function (req, res) {
  res.clearCookie('x-access-token');
  delete req.session;
  res.redirect('/');
  console.log("Cookie cleared");
});




//API
route.post('/register', controller.signup);
route.post('/login', controller.login);
route.put('/personalInfo/:id', controller.updateProfilePersonalInfo);


route.post('/api/users', controller.createUser);
route.get('/api/users', controller.find);
route.put('/api/users/:id', controller.updateUser);


route.post('/api/events', controller.createEvent);
route.get('/api/events', controller.findEvent);
route.put('/api/events/:id', controller.updateEvent);
route.delete('/api/events/:id', controller.deleteEvent);

module.exports = route